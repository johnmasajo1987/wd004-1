// console.log("hello");

let student = "Brandon";
console.log(student);

const day = "Monday";
// console.log(day);

// day = "Tuesday";
// console.log(day);

student = "Brenda";
console.log(student);

// Data Types
// 1. String - denoted by ""
// Series of Characters
// ex. "Brandon" "1234" "String" "cute1234"
// Basta may quotation marks

// 2. number - can be used in Math operations.
// ex. 2, 3.15, 0.25, 450

// boolean - true or false

console.log(typeof "Am I extreme?");
console.log(typeof "1234");
console.log(typeof 1234);
console.log(typeof "true");
console.log(typeof false);
// console.log(typeof whatami?); error
// console.log(typeof whatami); undefined

// Operators
// 1. Arithmetic Operators - +, -, *, /
const num1 = 10;
const num2 = 2;

console.log(num1 + num2);

// Modulo %

// Assignment Operator - denoted by =

// used to assign data types to variables
// +=, -=, *=, /=, %=l;

// Example

let output=0;
const num4 =5;
const num5=8;

output=output+num4;