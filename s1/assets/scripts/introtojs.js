/**
 * Give at least three reasons why you should learn javascript.
 */

1. Javascript is a good foundational language to learn before dealing w/ other languages.
2. Javascript is dynamic and weakly typed, making it easier to learn.
3. Javascript can be useful in classifying information within a site.

/**
 * In two sentences, state in your own words, what is your understanding about Javascript?
 */

1. Javascript is an interactive scripting language that needs a compiler to transform one language to machine code.
2. Javascript is a flexible and easy to learn scripting language that allows to  dynamically change content from w/in the browser w/o reloading.

/**
 * What is a variable? Give at least three examples.
 */

1. A variable is a value that corresponds to a certain function
Examples:
	1. let
	2. const
	3. var

/**
 * What are the data types we talked about today? Give at least two samples each.
 */

===> Here String examples: "username" "c1u2t3e4" Number examples: 0.122 1.5 Boolean examples: Correct Incorrect


/**
 * Write the correct data type of the following:
 */

1. "Number" String
2. 158 Number
3. pi Error
4. false Boolean
5. 'true' String
6. 'Not a String' String

/**
 * What are the arithmetic operators?
 */ Are symbols that perform a mathematical function. + - * / %

/**
 * What is a modulo? 
 */ Remainder

/**
 * Interpret the following code
 */

let number = 4;
number += 4; 

Q: What is the value of number? 8

----

let number = 4;
number -= 4;

Q: What is the value of number? 0

----

let number = 4;
number *= 4;

Q: What is the value of number? 16

----

let number = 4;
number /= 4;

Q: What is the value of number? 1

